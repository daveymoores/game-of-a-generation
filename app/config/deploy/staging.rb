set(:deploy_to)         { "/var/www/ign_cod" }
set(:releases_path)     { File.join(deploy_to, version_dir) }
set(:shared_path)       { File.join(deploy_to, shared_dir) }
set(:current_path)      { File.join(deploy_to, current_dir) }
set(:release_path)      { File.join(releases_path, release_name) }
set :keep_releases, 3
after "deploy:update", "deploy:cleanup"
set :branch, "prod"
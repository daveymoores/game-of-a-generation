set :application, 	"IGN_COD"
set :stages, 		%w(staging)
set :default_stage, "staging"
require 'capistrano/ext/multistage'

set :stage_dir, 	'app/config/deploy'
set :domain,		"igndev.jakepyne.net"
set :app_path,    	"app"
set :user,			"jake"
set :deploy_to,     "/var/www/ign_cod"
set :deploy_via,    :copy

set :repository,  "git@bitbucket.org:okjake/ign_cod.git"
set :scm,         :git

set :shared_files,      ["app/config/parameters.yml"]
set :shared_children,   [app_path + "/logs", web_path + "/uploads", web_path + "/data"]
set :model_manager, "doctrine"
set :use_composer,   true
set :composer_options, "--no-dev --verbose --prefer-dist --optimize-autoloader --no-progress --no-interaction"

set :update_vendors, true

role :web,        domain                         # Your HTTP server, Apache/etc
role :app,        domain, :primary => true       # This may be the same as your `Web` server
role :db,         domain, :primary => true  

set  :keep_releases,  3
set  :shared_files,      ["app/config/parameters.yml"]
set  :use_sudo,      false

logger.level = Logger::MAX_LEVEL
<?php

namespace Ign\VotingBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ign\VotingBundle\Entity\Votable;
use Ign\VotingBundle\Form\VotableType;

/**
 * Votable controller.
 *
 * @Route("/secure/votable")
 */
class VotableController extends Controller
{

    /**
     * Lists all Votable entities.
     *
     * @Route("/", name="secure_votable")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IgnVotingBundle:Votable')
                    ->findAllOrdered();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Votable entity.
     *
     * @Route("/", name="secure_votable_create")
     * @Method("POST")
     * @Template("IgnVotingBundle:Votable:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Votable();        
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $count = $em->getRepository('IgnVotingBundle:Votable')->getCount();
            $entity->setWeight($count+1);
            $em->persist($entity);
            $em->flush();

            //$this->dumpJson();
            $this->dumpHtml();

            return $this->redirect($this->generateUrl('secure_votable_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Votable entity.
    *
    * @param Votable $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Votable $entity)
    {
        $form = $this->createForm(new VotableType(), $entity, array(
            'action' => $this->generateUrl('secure_votable_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Votable entity.
     *
     * @Route("/new", name="secure_votable_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Votable();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Votable entity.
     *
     * @Route("/{id}", name="secure_votable_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IgnVotingBundle:Votable')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Votable entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Votable entity.
     *
     * @Route("/{id}/edit", name="secure_votable_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IgnVotingBundle:Votable')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Votable entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Votable entity.
    *
    * @param Votable $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Votable $entity)
    {
        $form = $this->createForm(new VotableType(), $entity, array(
            'action' => $this->generateUrl('secure_votable_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Votable entity.
     *
     * @Route("/{id}", name="secure_votable_update")
     * @Method("PUT")
     * @Template("IgnVotingBundle:Votable:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IgnVotingBundle:Votable')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Votable entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            //$this->dumpJson();
            $this->dumpHtml();
            return $this->redirect($this->generateUrl('secure_votable_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Votable entity.
     *
     * @Route("/{id}", name="secure_votable_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('IgnVotingBundle:Votable')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Votable entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        //$this->dumpJson();
        $this->dumpHtml();

        return $this->redirect($this->generateUrl('secure_votable'));
    }

    /**
     * Creates a form to delete a Votable entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('secure_votable_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

   /**
    * Sorting via AJAX
    * 
    * @Route("/sort", name="secure_votable_sort")
    * @Method("POST")
    */
    public function sortAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $items = $request->request->all();

        foreach($items as $id => $weight)
        {
            $entity = $em->getRepository('IgnVotingBundle:Votable')->find($id);
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Votable entity.');
            }
            $entity->setWeight($weight);
            $em->persist($entity);
            $em->flush();
        }
    
        //$this->dumpJson();
        $this->dumpHtml();

        return new Response('Saved!');

    }

    /**
     * Cache output
     */
    private function dumpJson()
    {
        $em = $this->getDoctrine()->getManager();
        $votables = $em->getRepository('IgnVotingBundle:Votable')
                    ->findAllOrdered();

        $data = array();

        foreach ($votables as $key=>$votable)
        {
            $data[$key] = array();
            $data[$key]['id'] = $votable->getId();
            $data[$key]['name'] = $votable->getName();
            $data[$key]['game'] = $votable->getGame();
            $data[$key]['description'] = $votable->getDescription();
            $data[$key]['image_path'] = $votable->getWebPath();
            $data[$key]['video_path'] = $votable->getVideoPath();
        }

        $path = $this->get('kernel')->getRootDir() . '/../web/data/';

        if (file_exists($path . 'votable.json'))
        {
            unlink($path . 'votable.json');
        }

        $file = fopen($path . 'votable.json', 'w');
        fwrite($file, json_encode($data));
        fclose($file);
    }


    /**
     * Cache output
     */
    private function dumpHtml()
    {
        $em = $this->getDoctrine()->getManager();
        $votables = $em->getRepository('IgnVotingBundle:Votable')
                    ->findAllOrdered();

        /*$data = array();

        foreach ($votables as $key=>$votable)
        {
            $data[$key] = array();
            $data[$key]['id'] = $votable->getId();
            $data[$key]['name'] = $votable->getName();
            $data[$key]['game'] = $votable->getGame();
            $data[$key]['description'] = $votable->getDescription();
            $data[$key]['image_path'] = $votable->getWebPath();
            $data[$key]['video_path'] = $votable->getVideoPath();
        }*/

        $path = $this->get('kernel')->getRootDir() . '/../web/data/';

        if (file_exists($path . 'moments.html'))
        {
            unlink($path . 'moments.html');
        }

        $file = fopen($path . 'moments.html', 'w');
        fwrite($file, $this->renderView('IgnVotingBundle:Votable:output.html.twig', array('votables' => $votables)) );
        fclose($file);
    }

    /**
     * Report showing number of downloads.
     *
     * @Route("/report", name="secure_votable_report")
     * @Method("GET")
     * @Template()
     */
    public function reportAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IgnVotingBundle:Votable')
                    ->findAllOrdered();

        return array(
            'entities' => $entities,
        );
    }
}
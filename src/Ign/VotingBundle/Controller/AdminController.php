<?php

namespace Ign\VotingBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ign\VotingBundle\Entity\Votable;
use Ign\VotingBundle\Form\VotableType;

/**
 * Votable Admin (extras) controller.
 *
 * @Route("/secure")
 */
class AdminController extends Controller
{

    private function escape($string)
    {
        $escaped = str_replace('"', '\'', $string);
        $escaped = '"' . $escaped . '"';
        return $escaped;
    }

    /**
     * Report showing number of points, in CSV
     *
     * @Route("/report/csv", name="secure_admin_report_csv")
     * @Method("GET")
     * @Template()
     */
    public function reportCsvAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IgnVotingBundle:Votable')->findAllRanked();

        $escaped = array();

        foreach ($entities as $key=>$entity)
        {
            $escaped[$key] = array();
            $escaped[$key]['id'] = $entity->getId();
            $escaped[$key]['name'] = $this->escape($entity->getName());
            $escaped[$key]['votes'] = $entity->getVotes();
        }

        $response = $this->render('IgnVotingBundle:Admin:reportcsv.html.twig', array('entities' => $escaped)); 

        $filename = "cod_moments_".date("Y_m_d_His").".csv"; 

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Description', 'Submissions Export');
        $response->headers->set('Content-Disposition', 'filename='.$filename);
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->headers->set('Pragma', 'no-cache');
        $response->headers->set('Expires', '0');

        return $response;
    }

    /**
     * Report showing number of points
     *
     * @Route("/report", name="secure_admin_report")
     * @Method("GET")
     * @Template()
     */
    public function reportAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IgnVotingBundle:Votable')
                    ->findAllRanked();

        return array(
            'entities' => $entities,
        );
    }
}
<?php

namespace Ign\VotingBundle\Controller;

use Ign\VotingBundle\Entity\Votable;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ApiController extends Controller
{

    /*
     * @Route("/api/votable")
     *
    public function listVotablesAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('IgnVotingBundle:Votable');
    
        $votables = $repository->findAllOrdered();
        $response = new Response(json_encode(
            array('votables' => $votables)
        ));

        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }*/



	/*
     * @Route("/api/votable/{id}/votes")
     *
    public function getVotesAction($id)
    {
    	$repository = $this->getDoctrine()
    		->getRepository('IgnVotingBundle:Votable');

    	$votable = $repository->find($id);

    	$response = new Response(json_encode(
    		array('votes' => $votable->getVotes())
    	));

    	$response->headers->set('Content-Type', 'application/json');

    	return $response;
    }*/

    /**
     * @Route("/api/vote")
     * @Method("POST")
     */
    public function voteAction(Request $request)
    {
    	$em = $this->getDoctrine()->getManager();
        $items = $request->request->all();

        foreach($items as $index => $id)
        {
            $entity = $em->getRepository('IgnVotingBundle:Votable')->find($id);
            if (!$entity) {
                throw $this->createNotFoundException('That moment doesn\'t exist');
            }
            $entity->setVotes($entity->getVotes() + $this->pointsByIndex($index));
            $em->persist($entity);
        }

        $em->flush();

        return new Response('OK!');
    }


    private function pointsByIndex($index)
    {
        if (!is_numeric($index))
        {
            return 0;
        }

        if ($index < 0 || $index > 9)
        {
            return 0;
        }

        return 10 - $index;
    }

}
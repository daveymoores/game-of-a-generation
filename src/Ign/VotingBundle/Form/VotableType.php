<?php

namespace Ign\VotingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VotableType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('name', 'text',
                array(
                    'attr' => array (
                        'class' => 'form-control'
                    )
                )
            )
            ->add('game', 'text',
                array(
                    'attr' => array (
                        'class' => 'form-control'
                    )
                )
            )
            ->add('description', 'textarea',
                array(
                    'attr' => array (
                        'class' => 'form-control'
                    )
                )
            )
            ->add('video_path', 'textarea',
                array(
                    'attr' => array (
                        'class' => 'form-control'
                    )
                )
            )
            ->add('file', 'file', 
                array(
                    'label' => 'Image',
                    'required' => false,
                    'attr' => array (
                        'class' => 'form-control',
                    )
                )
            )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ign\VotingBundle\Entity\Votable'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ign_votingbundle_votable';
    }
}

<?php
namespace Ign\VotingBundle\Entity;

use Doctrine\ORM\EntityRepository;

class VotableRepository extends EntityRepository
{
	public function getCount()
	{
		
		$query = $this->createQueryBuilder('v')
		    ->select('COUNT(v.id)') 
		    ->getQuery();

		return $query->getSingleScalarResult();

	}

	public function findAllOrdered()
	{
		
		return $this->getEntityManager()
            ->createQuery(
                'SELECT v FROM IgnVotingBundle:Votable v ORDER BY v.weight ASC'
            )
            ->getResult();
		
	}

	public function findAllRanked()
	{
		return $this->getEntityManager()
            ->createQuery(
                'SELECT v FROM IgnVotingBundle:Votable v ORDER BY v.votes DESC'
            )
            ->getResult();
	}
}
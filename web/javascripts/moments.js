
Moments = {
	
	/* 
	 * General config settings 
	 */
	settings : {
		api_dir 	: '../api/',
      data_dir : '../web/data/',
		production 	: false,
		list		: $('#sortable1')
	},

	/* Prevent console.log in production */
	log : function(toLog) {
		if (!Moments.settings.production) {
			console.log(toLog);
		}
	},

	/* Get all of the moments and pass them to callback */
	loadAll: function (attach) {
		attach.load( Moments.settings.data_dir + 'moments.html' );
   },

   submitSelection: function(fail, win) {
   	var selection = {};

   	// Iterate the final selection list and build data object
   	Moments.settings.list.find('.mom_container.dragged').each(function(index, value){
   		selection[index] = $(value).find('img').data('momid');
   	});

    	$.ajax({
   		url: Moments.settings.api_dir + 'vote',
   		data: selection,
   		method: "POST",
   		error: function(jqXHR, textStatus, err) {
   			fail();
   		},
   		success: function(data, textStatus, jqXHR) {
            win();
   		}
   	});
   }
}